Instructions: 
You have learned about git, GitHub and GitLab. In this lab, you will have hands-on experience by using them in your machine. 
Please take necessary screenshot of all the git commands and its execution at each of the steps below.
- Step 1: Install git in your local machine (If it is already done, skip this step)
- Step 2: Build a ML model for Breast Cancer Wisconsin (Diagnostic) Data set in jupyter notebook. https://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+(diagnostic)
- Step 3: Please provide screenshots for various stages of the design process (importing data, training, evaluation …)
- Step 4: Upload your model (Python script, let’s called it <yourname>_model_v1) to GitHub. Provide screenshot of all your git commands and your command prompt showing success of commit of your model files in the remote host.
- Step 5: Create a branch in your repo and upload another ML model (may be using a different algorithm and named the file: <yourname>_model_v2) of your choice for the same dataset into that branch.
- Step 6: Navigate to your newly created branch and provide screenshot showing status of your repo.
- Step 7: Provide a screenshot showing your log of activities and perform your final commit.
- Step 8: Provide a description of your program in the README.md file.
- Step 9: Make your repo public and share the link of your repo for check.

